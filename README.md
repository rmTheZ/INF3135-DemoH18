# Solution des laboratoires INF3135 Session Hiver 2018

Ce repertoire contient quelques solutions des differents laboratoires et certaines ressources utiles

## Environnement Linux

* Installation d'une machine virtuelle Linux avec [VirtualBox](https://www.youtube.com/watch?v=1zfO-Fhqyb8)
* Installation du bash subsystem sur Windows [Voir ce lien](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
* Choisissez une distribution basée sur **Debian** (pour avoir les mêmes commandes que j'utiliserai) tel que
  * [Ubuntu](https://www.ubuntu.com/download)
  * [Mint](https://linuxmint.com/download.php)
  * [Debian](https://www.debian.org/CD/)

## Fonctions **Unix** utilisées
* `$  cd` -> Change Directory
  * `$  cd ../` -> Revient au dossier parent
* `$  mkdir` -> Make Directory
* `$  ls` -> List Directory
  * `$  ls -l` -> List directory version longue
  * `$  ls -a` -> List directory incluant les fichiers cachés
* `$  rm file` -> Supprime file
  * `$  rm -r folder` -> Supprime folder récursivement
* `$  rmdir folder` -> Supprime folder (si vide)
* `$  cat file` -> Affiche le contenu de file dans la console
* `$  touch file` -> Créé file (vide)
* `$  vim file` -> Ouvre (ou créé) file dans vim

## Ressources

* [Une bonne cheatsheet pour le format **Markdown**](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [Une bonne cheatsheet pour les commandes **UNIX**](http://cheatsheetworld.com/programming/unix-linux-cheat-sheet/)
