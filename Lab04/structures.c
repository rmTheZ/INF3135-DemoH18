#include "stdio.h"
#include <math.h>

struct Point { // Un point en 2D
    double x;  // Sa coordonnée x
    double y;  // Sa coordonnée y
};

struct Segment {
    struct Point p1;
    struct Point p2;
};

struct Triangle {
    struct Point points[3];
};

void initialiserSegment(struct Segment *segment, double x1, double y1, double x2, double y2);
void initialiserSegment2(struct Segment *segment, double x1, double y1, double x2, double y2);
void initialiserSegment3(struct Segment *segment, double x1, double y1, double x2, double y2);
double longueurSegment(const struct Segment *segment);
void initialiserTriangle(struct Triangle *triangle, double x1, double y1, double x2, double y2, double x3, double y3);

int main(int argc, char const *argv[])
{
  // Initialisation d'un segment
  struct Segment segment;
  initialiserSegment(&segment, 1.0, 2.0, 3.0, 4.0);
  // initialiserSegment2(&segment, 1.0, 2.0, 3.0, 4.0);
  // initialiserSegment3(&segment, 1.0, 2.0, 3.0, 4.0);
  printf("Point 1 => (%.2f,%.2f)  | Point 2 => (%.2f,%.2f)\n", segment.p1.x, segment.p1.y, segment.p2.x, segment.p2.y);

  printf("Longueur du segment : %.2f\n", longueurSegment(&segment));

  return 0;
}

void initialiserSegment(struct Segment *segment, double x1, double y1, double x2, double y2){
  struct Point p1 = {x1, y1};
  struct Point p2 = {x2, y2};
  *segment = (struct Segment) {p1, p2};
}

void initialiserSegment2(struct Segment *segment, double x1, double y1, double x2, double y2){
  struct Point p1 = {x1, y1};
  struct Point p2 = {x2, y2};
  segment->p1 = p1;
  segment->p2 = p2;
}

void initialiserSegment3(struct Segment *segment, double x1, double y1, double x2, double y2){
  struct Point p1 = {x1, y1};
  struct Point p2 = {x2, y2};
  (*segment).p1 = p1;
  (*segment).p2 = p2;
}

double longueurSegment(const struct Segment *segment){
  double a = segment->p2.x - segment->p1.x;
  double b = segment->p2.y - segment->p1.y;
  a *= a;
  b *= b;
  double c = sqrt(a+b);
  return c;
}
