#include "stdio.h"
#include "string.h"

int main(int argc, char const *argv[])
{
  int i;
  char chaine[200] = ""; // chaine assez longue pour contenir la concatenation
  for (i = argc-1; i > 0; --i)
  {
    strcat(chaine, argv[i]);
  }
  printf("%s\n", chaine);
  return 0;
}