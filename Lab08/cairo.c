#include <cairo.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char * argv[]){
	int width;
	int height;
	int space;

	if(argc != 4){
		printf("ERREUR, pas assez d'arguments.");
		exit(1);
	}else{
		
	}

	cairo_surface_t *surface;
	cairo_t *cr;
	
	double xc = 128.0;
double yc = 128.0;
double radius = 100.0;
double angle1 = 45.0  * (0/180.0);  /* angles are specified */
double angle2 = 180.0 * (0/180.0);  /* in radians           */

cairo_set_line_width (cr, 10.0);
cairo_arc (cr, xc, yc, radius, angle1, angle2);
cairo_stroke (cr);

/* draw helping lines */
cairo_set_source_rgba (cr, 1, 0.2, 0.2, 0.6);
cairo_set_line_width (cr, 6.0);

cairo_arc (cr, xc, yc, 10.0, 0, 2);
cairo_fill (cr);

cairo_arc (cr, xc, yc, radius, angle1, angle1);
cairo_line_to (cr, xc, yc);
cairo_arc (cr, xc, yc, radius, angle2, angle2);
cairo_line_to (cr, xc, yc);
cairo_stroke (cr);	

	//surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, 120, 120);
	//cr = cairo_create (surface);

	//cairo_set_source_rgb (cr, 0, 0, 0);	
	

}
