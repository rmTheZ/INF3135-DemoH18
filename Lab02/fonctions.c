#include <stdio.h>
#include <string.h>

int somme(int tab[], int size);
int estTri(int tab[], int size);
int elementPlusFrequent(int tab[], int size);
int nombreOccurence(char *chaine,char caractere);

int main(int argc, char const *argv[])
{
    // Tableaux sur lesquels on va appeler nos fonctions
    int tab1[] = {1,2,3,4,5,4};
    int tab2[] = {5,6,2,8,4,6,5,6};

    // Longueur des tableaux
    int lengthTab1 = sizeof(tab1)/sizeof(int);
    int lengthTab2 = sizeof(tab2)/sizeof(int);

    // Affichage de Hello + Argument
    printf("%s\n", "--- Arguments");
    if(argc != 2){
        printf("%s\n", "Erreur! Un seul argument permis!");
    }else{
        printf("Hello %s\n", argv[1]);
    }

    // Somme des tableaux
    printf("%s\n", "--- somme");
    printf("Somme du tab1: %d\n", somme(tab1,lengthTab1));
    printf("Somme du tab2: %d\n", somme(tab2,lengthTab2));

    // Tri des tableaux
    printf("%s\n", "--- estTri");
    if(estTri(tab1, lengthTab1)){
        printf("%s\n", "Le tableau tab1 est ordonne!");
    }else{
        printf("%s\n", "Le tableau tab1 n'est pas ordonne!");
    }
    if(estTri(tab2, lengthTab2)){
        printf("%s\n", "Le tableau tab2 est ordonne!");
    }else{
        printf("%s\n", "Le tableau tab2 n'est pas ordonne!");
    }

    // Element plus frequent
    printf("%s\n", "--- elementPlusFrequent");
    printf("L'element le plus frequent du tab1 est : %d\n", elementPlusFrequent(tab1,lengthTab1));
    printf("L'element le plus frequent du tab2 est : %d\n", elementPlusFrequent(tab2,lengthTab2));

    // Nombre d'occurence d'un caractere
    printf("%s\n", "--- nombreOccurence");
    char *chaine = "Voici une chaine de caractere avec quelques mots";
    printf("Il y a %d fois le caractere %c\n", nombreOccurence(chaine,'e'), 'e');
    return 0;
}

int somme(int tab[], int size){
    int i;
    int total = 0;
    for (i = 0; i < size; ++i)
    {
        total += tab[i];
    }
    return total;
}

int estTri(int tab[], int size){
    int i;
    for (i = 0; i < size-1; ++i)
    {
        if(tab[i] >= tab[i+1]){
            return 0;
        }
    }
    return 1;
}

int elementPlusFrequent(int tab[], int size){
    int plusFrequent = tab[0];
    int nombreOccurence = 0;
    int i, j;
    for (i = 0; i < size; ++i)
    {
        int count = 0;
        for (j = 0; j < size; ++j)
        {
            if(tab[i] == tab[j]){
                count++;
            }
        }
        if(nombreOccurence < count){
            nombreOccurence = count;
            plusFrequent = tab[i];
        }
    }
    return plusFrequent;
}

int nombreOccurence(char *chaine,char caractere){
    int count = 0;
    int i;
    for (i = 0; chaine[i] != '\0'; ++i)
    {
        if(chaine[i] == caractere) count++;
    }
    return count;
}