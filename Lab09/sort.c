#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int cmp(const void *a, const void *b);
void print(char** array, int size);


void print(char** array, int size){
  int i;
  for (i = 0; i < size; ++i){
      printf("%s\n", array[i]);
    }
}


int main(int argc, char const *argv[])
{
  char *buffer = malloc(1000);
  FILE *f;
  size_t read;

  // Facon 1
  // f = fopen("capitals.txt", "r");
  // if(f){
  //   while((read = fread(buffer, 1, 1000, f)) > 0){
  //     printf("%d %s\n", read, buffer);
  //   }
  //   fclose(f);
  // }

  // Facon 2
  char *array[500];
  char line[1000];
  unsigned int count = 0;

  f = fopen("capitals.txt", "r");
  if(f){
    while(fgets(line, sizeof(line), f)){
      array[count] = malloc(strlen(line));
      strtok(line, "\n"); // Remove \n
      strcpy(array[count], line);
      count++;
    }
  }
  return 0;
}
